<?php require_once("header.php"); ?>
<?php require_once ("initialisation.php") ?>
<!-- Begin page content -->
    <main role="main" class="container mt-5" role="main">
        <div class="container">
            <h1 class="mt-5 pt-5">Évènements</h1>
            <form method="post" class="form-signin">
                </select>
                <?php
                echo '<select class="mr-3" name="resource" >';
                $reponse = $bdd->prepare('SELECT rub57 FROM eventresource4');
                $reponse->execute();
                $donnees = $reponse->fetchAll();
                if ($reponse->rowCount() > 1) {
                    foreach ($donnees as $donnee) {
                        echo '<option name="rub57" value="' . $donnee['rub57'] . '">';
                        echo $donnee['rub57'];
                        echo "</option>";

                    }
                }
                echo '</select>';

                echo '<select class="mr-3" name="activite" >';
                $reponse2 = $bdd->prepare('SELECT rub1 FROM eventresource2');
                $reponse2->execute();
                $donnees = $reponse2->fetchAll();
                if ($reponse2->rowCount() > 1) {
                    foreach ($donnees as $donnee) {
                        echo '<option name="rub1" value="' . $donnee['rub1'] . '">';
                        echo $donnee['rub1'];
                        echo "</option>";

                    }
                }
                echo '<option name="rub1" value="LIKE%" >Tous</option>';
                echo '</select>'
                ?>
                <input class="mr-3" type="date" name="date1" value="<?php if (isset($_POST['go'])) {
                    echo $_POST['date1'];
                } ?>">
                <input class="mr-3 ml-3" type="date" name="date2" value="<?php if (isset($_POST['go'])) {
                    echo $_POST['date2'];
                } ?>">
                <button type="submit" name="go" class="btn btn-outline-secondary">VALIDER</button>
            </form>
                <div class="table-responsive">
                    <?php
                    if (isset($_POST['go'])) {
                        $params = [
                            ':date1' => $_POST['date1'],
                            ':date2' => $_POST['date2'],
                            ':resource' => $_POST['resource'],
                            ':activite' => $_POST['activite']
                        ];
                        echo '<h2>Résultat:</h2>
                        <TABLE BORDER="1" class="table table-striped table-sm">
                        <tr>
                        <th>Ressource</th>
                        <th>Activité</th>
                        <th>Commentaire</th>
                        <th>Date début</th>
                        <th>Date fin</th>
                        </tr>';
                        $request = $bdd->prepare('select eventresource4.rub57 as ressource, eventresource2.rub1 as activite, comments, BEGINDATE, ENDDATE 
                                                            from eventresource2, eventresource4, planningevent 
                                                            where planningevent.resource4 = eventresource4.id and eventresource2.rub1 = :activite and eventresource4.rub57 = :resource and BEGINDATE BETWEEN :date1 and :date2');
                        $request->execute($params);
                        $data = $request->fetchAll();
                        if (!$request) {
                            print_r($bdd->errorInfo());
                        } else {
                            if ($request->rowCount() > 1) {
                                foreach ($data as $date) {
                                    echo '<TR>';

                                    echo '<TD class="col1">' . $date['ressource'] . '</TD>';

                                    echo '<TD class="col1">' . $date['activite'] . '</TD>';

                                    echo '<TD class="col1">' . $date['comments'] . '</TD>';


                                    echo '<TD class="col1">' . $date['BEGINDATE'] . '</TD>';


                                    echo '<TD class="col1">' . $date['ENDDATE'] . '</TD>';

                                    echo '</TR>';
                                }
                            } else {
                                var_dump($data);
                            }
                        }
                    }
                    echo '</TR></TABLE>';
                    ?>
                </div>
            </div>
    </main>
<?php require_once("footer.php"); ?>

