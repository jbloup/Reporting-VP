<?php require_once ("header.php"); ?>
<!-- Begin page content -->
    <main role="main" class="container mt-5" role="main">
        <div class="container">
            <h1 class="mt-5 pt-5">Evènements supprimés</h1>
            <form method="post" class="form-signin">
                <input class="mr-3" type="date" name="date1" value="<?php if (isset($_POST['go'])) {
                    echo $_POST['date1'];
                } ?>">
                <input class="mr-3 ml-3" type="date" name="date2" value="<?php if (isset($_POST['go'])) {
                    echo $_POST['date2'];
                } ?>"> 
                <button type="submit" name="go" class="btn btn-outline-secondary">VALIDER</button>
            </form>
                <div class="table-responsive">
                    <?php
                    try
                    {
                        $user = "root";
                        $pass = "root";
                        $bdd = new PDO('mysql:host=localhost:3306;dbname=boresa', $user, $pass);
                    }
                    catch (Exception $e)
                    {
                        die('Erreur : ' . $e->getMessage());
                    }
                    ?>
                    <?php
                    if (isset($_POST['go'])) {
                        $params = [
                            ':date1' => $_POST['date1'],
                            ':date2' => $_POST['date2']
                        ];
                        echo '<h2>Résultat:</h2>
                        <TABLE BORDER="1" class="table table-striped table-sm">
                        <tr>
                        <th>entityClassName</th>
                        <th>deletelogin</th>
                        <th>deleteDate</th>
                        <th>createLogin</th>
                        <th>createdate</th>
                        <th>modifyLogin</th>
                        <th>modifyDate</th>
                        </tr>';
                        $request = $bdd->prepare('SELECT deleteDate, deleteLogin, createDate, createLogin, entityClassName, modifyLogin, modifyDate from history where deleteDate is not NULL AND deleteDate BETWEEN :date1 AND :date2');
                        $request->execute($params);
                        $data = $request->fetchAll();
                        if ($request->rowCount() > 1) {
                            foreach ($data as $date) {
                                echo '<TR>';
                                echo '<TD class="col1">' . $date['entityClassName'] . '</TD>';

                                echo '<TD class="col1">' . $date['deleteLogin'] . '</TD>';

                                echo '<TD class="col1">' . $date['deleteDate'] . '</TD>';


                                echo '<TD class="col1">' . $date['createLogin'] . '</TD>';


                                echo '<TD class="col1">' . $date['createDate'] . '</TD>';

                                echo '<TD class="col1">' . $date['modifyLogin'] . '</TD>';


                                echo '<TD class="col1">' . $date['modifyDate'] . '</TD>';
                                echo '</TR>';
                            }
                        }
                    }
                    echo '</TR></TABLE>';
                    ?>
                </div>
            </div>
    </main>
<?php require_once ("footer.php"); ?>

